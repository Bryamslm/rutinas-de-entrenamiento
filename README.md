# Rutinas de Entrenamiento

<p><h4>Consiste en la creación de rutunas de entrenamiento con una cantidad definidad de ejercicios ya establecidad donde se puede escoger cuales agregar a la rutina para crearla.<\h4>
<\p>

<h3>Acciones que permite realizar la App:<\h3>

<ul>
<li>Registrarse<\li>
<li>Iniciar sesión<\li>
<li>Ejecutar rutina<\li>
<li>Registrar resultados de rutina efectuada<\li>
<li>Crear rutina<\li>
<li>Ver ejercicio<\li>
<li>Ver historial de usuario<\li>
<li>Modificar y borrar rutina<\li>

