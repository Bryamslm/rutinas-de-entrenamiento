import java.awt.Image;
import java.awt.Toolkit;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.LinkedList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import static javax.swing.JOptionPane.INFORMATION_MESSAGE;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Usuario7
 */
public class EjecutarRutinaFrame extends javax.swing.JFrame {

    /**
     * Creates new form EjecutarRutinaFrame
     */
    public EjecutarRutinaFrame(Usuarios usuario, String nombreRutina) {
        Toolkit iconoV= Toolkit.getDefaultToolkit();
        Image urlIcono = iconoV.getImage("./src/Graficas/LOGO.png");
        this.setIconImage(urlIcono);
        
        initComponents();
        Boxejercicios.addItem("Seleccionar");
        LinkedList<Rutina> rutinas=new LinkedList<Rutina>();
        nUsuario = usuario;
        rutinas= nUsuario.getRutinas();
        contador=-1;
        for(Rutina r: rutinas){
            contador+=1;
            if(r.getNombreRutina().equals(nombreRutina)){
                nRutina= r;
                break;
            }
            
        }
        this.Nombre.setText(nRutina.getNombreRutina());
        cargarEjercicios();
        this.setLocationRelativeTo(null);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        foto1 = new javax.swing.JLabel();
        foto2 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        tiempo = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        repeticiones = new javax.swing.JTextField();
        guardarCambios = new javax.swing.JButton();
        btcancelar = new javax.swing.JButton();
        Nombre = new javax.swing.JTextField();
        Boxejercicios = new javax.swing.JComboBox<>();
        jLabel7 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        metaRepeticiones = new javax.swing.JLabel();
        metaTiempo = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        metaPeso = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        peso = new javax.swing.JTextField();
        btResultadoEj = new javax.swing.JButton();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu2 = new javax.swing.JMenu();
        botonSalir = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setBackground(new java.awt.Color(156, 56, 57));

        jPanel1.setBackground(new java.awt.Color(156, 50, 57));

        jLabel1.setBackground(new java.awt.Color(255, 255, 255));
        jLabel1.setFont(new java.awt.Font("Arial", 3, 24)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setText("Ejecutar Rutina");

        foto1.setBackground(new java.awt.Color(204, 204, 204));

        foto2.setBackground(new java.awt.Color(204, 204, 204));

        jLabel2.setBackground(new java.awt.Color(255, 255, 255));
        jLabel2.setFont(new java.awt.Font("Arial", 3, 18)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(255, 255, 255));
        jLabel2.setText("Ejercicio:");

        jLabel3.setBackground(new java.awt.Color(255, 255, 255));
        jLabel3.setFont(new java.awt.Font("Arial", 3, 18)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(255, 255, 255));
        jLabel3.setText("Tiempo:");

        tiempo.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        tiempo.setHorizontalAlignment(javax.swing.JTextField.CENTER);

        jLabel4.setBackground(new java.awt.Color(255, 255, 255));
        jLabel4.setFont(new java.awt.Font("Arial", 3, 18)); // NOI18N
        jLabel4.setForeground(new java.awt.Color(255, 255, 255));
        jLabel4.setText("Peso:");

        repeticiones.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        repeticiones.setHorizontalAlignment(javax.swing.JTextField.CENTER);

        guardarCambios.setBackground(new java.awt.Color(112, 128, 144));
        guardarCambios.setFont(new java.awt.Font("Arial", 3, 14)); // NOI18N
        guardarCambios.setForeground(new java.awt.Color(255, 255, 255));
        guardarCambios.setText("Guardar en historial");
        guardarCambios.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        guardarCambios.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                guardarCambiosActionPerformed(evt);
            }
        });

        btcancelar.setBackground(new java.awt.Color(112, 128, 144));
        btcancelar.setFont(new java.awt.Font("Arial", 3, 14)); // NOI18N
        btcancelar.setForeground(new java.awt.Color(255, 255, 255));
        btcancelar.setText("Regresar");
        btcancelar.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        btcancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btcancelarActionPerformed(evt);
            }
        });

        Nombre.setEditable(false);
        Nombre.setBackground(new java.awt.Color(156, 50, 57));
        Nombre.setFont(new java.awt.Font("Arial", 3, 14)); // NOI18N
        Nombre.setForeground(new java.awt.Color(255, 255, 255));
        Nombre.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        Nombre.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                NombreActionPerformed(evt);
            }
        });

        Boxejercicios.setBackground(new java.awt.Color(204, 255, 255));
        Boxejercicios.setToolTipText("Seleccionar");
        Boxejercicios.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BoxejerciciosActionPerformed(evt);
            }
        });

        jLabel7.setBackground(new java.awt.Color(255, 255, 255));
        jLabel7.setFont(new java.awt.Font("Arial", 3, 18)); // NOI18N
        jLabel7.setForeground(new java.awt.Color(255, 255, 255));
        jLabel7.setText("Meta:");

        jLabel5.setBackground(new java.awt.Color(255, 255, 255));
        jLabel5.setFont(new java.awt.Font("Arial", 3, 18)); // NOI18N
        jLabel5.setForeground(new java.awt.Color(255, 255, 255));
        jLabel5.setText("Meta:");

        metaRepeticiones.setFont(new java.awt.Font("Arial", 3, 14)); // NOI18N
        metaRepeticiones.setForeground(new java.awt.Color(255, 255, 255));

        metaTiempo.setFont(new java.awt.Font("Arial", 3, 14)); // NOI18N
        metaTiempo.setForeground(new java.awt.Color(255, 255, 255));

        jLabel6.setBackground(new java.awt.Color(255, 255, 255));
        jLabel6.setFont(new java.awt.Font("Arial", 3, 18)); // NOI18N
        jLabel6.setForeground(new java.awt.Color(255, 255, 255));
        jLabel6.setText("Repeticiones:");

        metaPeso.setFont(new java.awt.Font("Arial", 3, 14)); // NOI18N
        metaPeso.setForeground(new java.awt.Color(255, 255, 255));

        jLabel8.setBackground(new java.awt.Color(255, 255, 255));
        jLabel8.setFont(new java.awt.Font("Arial", 3, 18)); // NOI18N
        jLabel8.setForeground(new java.awt.Color(255, 255, 255));
        jLabel8.setText("Meta:");

        peso.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        peso.setHorizontalAlignment(javax.swing.JTextField.CENTER);

        btResultadoEj.setBackground(new java.awt.Color(112, 128, 144));
        btResultadoEj.setFont(new java.awt.Font("Arial", 3, 14)); // NOI18N
        btResultadoEj.setForeground(new java.awt.Color(255, 255, 255));
        btResultadoEj.setText("Guardar Resultado");
        btResultadoEj.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        btResultadoEj.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btResultadoEjActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap(36, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(18, 18, 18)
                        .addComponent(guardarCambios, javax.swing.GroupLayout.PREFERRED_SIZE, 156, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(42, 42, 42)
                        .addComponent(btcancelar, javax.swing.GroupLayout.PREFERRED_SIZE, 76, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                        .addGroup(jPanel1Layout.createSequentialGroup()
                            .addComponent(foto2, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGap(28, 28, 28)
                            .addComponent(foto1, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(jPanel1Layout.createSequentialGroup()
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                .addComponent(jLabel3)
                                .addComponent(jLabel2)
                                .addComponent(jLabel4)
                                .addComponent(jLabel6))
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(Boxejercicios, javax.swing.GroupLayout.PREFERRED_SIZE, 143, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGroup(jPanel1Layout.createSequentialGroup()
                                    .addComponent(peso, javax.swing.GroupLayout.PREFERRED_SIZE, 143, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(jLabel8)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(metaPeso, javax.swing.GroupLayout.PREFERRED_SIZE, 61, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGroup(jPanel1Layout.createSequentialGroup()
                                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addGroup(jPanel1Layout.createSequentialGroup()
                                            .addGap(147, 147, 147)
                                            .addComponent(jLabel5))
                                        .addComponent(repeticiones, javax.swing.GroupLayout.PREFERRED_SIZE, 143, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(metaRepeticiones, javax.swing.GroupLayout.PREFERRED_SIZE, 61, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGroup(jPanel1Layout.createSequentialGroup()
                                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(tiempo, javax.swing.GroupLayout.PREFERRED_SIZE, 143, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(btResultadoEj, javax.swing.GroupLayout.PREFERRED_SIZE, 131, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(jLabel7)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(metaTiempo, javax.swing.GroupLayout.PREFERRED_SIZE, 61, javax.swing.GroupLayout.PREFERRED_SIZE))))))
                .addContainerGap(36, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel1)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(21, 21, 21)
                        .addComponent(Nombre, javax.swing.GroupLayout.PREFERRED_SIZE, 137, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(147, 147, 147))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(Nombre, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(51, 51, 51)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(foto1, javax.swing.GroupLayout.PREFERRED_SIZE, 123, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(Boxejercicios, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel2)))
                    .addComponent(foto2, javax.swing.GroupLayout.PREFERRED_SIZE, 123, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(repeticiones, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(metaRepeticiones, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(jLabel6)
                                .addComponent(jLabel5)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(peso, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(jLabel4)
                                .addComponent(jLabel8))
                            .addComponent(metaPeso, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(jLabel3)
                                .addComponent(tiempo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(jLabel7))
                            .addComponent(metaTiempo, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addGap(18, 18, 18)
                .addComponent(btResultadoEj, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(guardarCambios, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btcancelar, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(24, Short.MAX_VALUE))
        );

        jMenuBar1.setBackground(new java.awt.Color(156, 56, 57));
        jMenuBar1.setForeground(new java.awt.Color(255, 255, 255));

        jMenu2.setForeground(new java.awt.Color(255, 255, 255));
        jMenu2.setText("Archivo");

        botonSalir.setText("Salir");
        botonSalir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonSalirActionPerformed(evt);
            }
        });
        jMenu2.add(botonSalir);

        jMenuBar1.add(jMenu2);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btcancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btcancelarActionPerformed
        // TODO add your handling code here:
        MenuUsuario menu= new MenuUsuario(nUsuario);
        menu.setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_btcancelarActionPerformed

    private void BoxejerciciosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BoxejerciciosActionPerformed
        // TODO add your handling code here:
        String ejercicioNombre=(String)Boxejercicios.getSelectedItem();
        
        if("Seleccionar".equals(ejercicioNombre)){
            repeticiones.setText("");
            peso.setText("");
            tiempo.setText("");
            repeticiones.setEditable(false);
            peso.setEditable(false);
            tiempo.setEditable(false);
            guardarCambios.setEnabled(false);
            btResultadoEj.setEnabled(false);
            foto1.setIcon(null);
            foto2.setIcon(null);
        }else{//Sino está "Seleccionar" es porque hay otra opción seleccionada

            LinkedList<Ejercicio> ejercicios = newRutina.getEjercicios();

            for(Ejercicio e: ejercicios){
                if(e.getNombre().equals(ejercicioNombre)){

                    guardarCambios.setEnabled(true);
                    btResultadoEj.setEnabled(true);
                    repeticiones.setText(String.valueOf(e.getRepeticiones()));
                    peso.setText(String.valueOf(e.getPeso()));
                    tiempo.setText(String.valueOf(e.getSegundos()));
                    
                    metaRepeticiones.setText(String.valueOf(e.getRepeticiones()));
                    metaPeso.setText(String.valueOf(e.getPeso()));
                    metaTiempo.setText(String.valueOf(e.getSegundos()));

                    if(e.getRepeticiones()==0){
                        repeticiones.setEditable(false);
                    }else{
                        repeticiones.setEditable(true);
                    }
                    if(e.getSegundos()==0){
                        tiempo.setEditable(false);
                    }else{
                        tiempo.setEditable(true);
                    }
                    if(e.getPeso()==-1){
                        peso.setText("0");
                         metaPeso.setText("0");
                        peso.setEditable(false);
                    }else{
                        peso.setEditable(true);
                    }
                    
                    foto1.setIcon(new javax.swing.ImageIcon(e.getLinkFoto1()));
                    foto2.setIcon(new javax.swing.ImageIcon(e.getLinkFoto2()));
                    ejercicio=e;
                    break;
                    
                }
            }

        }
    }//GEN-LAST:event_BoxejerciciosActionPerformed

    private void btResultadoEjActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btResultadoEjActionPerformed
        // TODO add your handling code here:
        ejercicio.setRepeticiones(Integer.parseInt(repeticiones.getText().trim()));
        ejercicio.setSegundos(Integer.parseInt(tiempo.getText().trim()));
        ejercicio.setPeso(Integer.parseInt(peso.getText().trim()));
        JOptionPane.showMessageDialog(null, "Resultado de ejercicio ejercutado a sido guardado");
    }//GEN-LAST:event_btResultadoEjActionPerformed

    private void guardarCambiosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_guardarCambiosActionPerformed
        // TODO add your handling code here:
        
        File link= new File("usuariosRegistrados.txt");

        try {
            int tiempo=0;
            int peso=0;
            int repeticiones=0;
            Boolean mejorTiempo=true;
            Boolean mejorPeso=true;
            Boolean mejorRepeticiones=true;
            LinkedList<Ejercicio> ejercicios=newRutina.getEjercicios();
            for(Ejercicio e: ejercicios){
                tiempo+=e.getSegundos();
                peso+=e.getPeso();
                repeticiones+=e.getRepeticiones();
            }
            ObjectInputStream lector = new ObjectInputStream(new FileInputStream(link));
            LinkedList<Usuarios> usuarios= (LinkedList<Usuarios>) lector.readObject();
            for(Usuarios u: usuarios){
                if(u.getUsuario().equals(nUsuario.getUsuario())){
                    LinkedList<Historial> historiales=u.getHistoriales();
                    for(Historial h: historiales){
                        if(h.getNombre().equals(nRutina.getNombreRutina())){
                           LinkedList<Rutina>rutinas=h.getRutinas();
                           for(Rutina r: rutinas){
                               int tiempo1=0;
                               int peso1=0;
                               int repeticiones1=0;
                               LinkedList<Ejercicio> ejercicios1=r.getEjercicios();
                                for(Ejercicio e: ejercicios1){
                                tiempo1+=e.getSegundos();
                                peso1+=e.getPeso();
                                repeticiones1+=e.getRepeticiones();
                                }
                                if(tiempo1>=tiempo){
                                    mejorTiempo=false;
                                }
                                if(peso1>=peso){
                                    mejorPeso=false;
                                }
                                if(repeticiones1>=repeticiones){
                                    mejorRepeticiones=false;
                                }
                                if(!mejorTiempo && !mejorPeso && !mejorRepeticiones){
                                    break;
                                }    
                            }
                            if(mejorPeso || mejorRepeticiones || mejorTiempo){
                                h.setMejorRutina(newRutina);
                                String strPeso=" ";
                                String strRepe=" ";
                                String strTiempo=" ";
                                if(mejorPeso)
                                    strPeso="Peso";
                                if(mejorRepeticiones)
                                    strRepe="Repeticiones";
                                if(mejorTiempo)
                                    strTiempo="Tiempo";
                                JOptionPane.showMessageDialog(rootPane, "¡Ha logrado una mejor marca en:\n"+strPeso+"  "+strRepe+"  "+strTiempo, "Nuevo Record", INFORMATION_MESSAGE);
                            }
                            else{
                                h.setRutina(newRutina);
                            }
                        }
                    }
                    nUsuario=u;
                    break;
                }
            }
            ObjectOutputStream escritor = new ObjectOutputStream(new FileOutputStream(link));
            escritor.writeObject(usuarios);

        } catch (IOException | ClassNotFoundException e) {
            Logger.getLogger(CrearRutinaFrame.class.getName()).log(Level.SEVERE, null, e);
        }

        JOptionPane.showMessageDialog(null, "Rutina Ejecutada correctamente");
    }//GEN-LAST:event_guardarCambiosActionPerformed

    private void botonSalirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonSalirActionPerformed
        // TODO add your handling code here:
        System.exit(0);
    }//GEN-LAST:event_botonSalirActionPerformed

    private void NombreActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_NombreActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_NombreActionPerformed
    private void cargarEjercicios(){
        
        LinkedList<Ejercicio> ejercicios= nRutina.getEjercicios();
        
        newRutina=new Rutina(nRutina.getNombreRutina());
        
        
        for(Ejercicio e: ejercicios){
            
            newRutina.agregarEjercicio(e);
            Boxejercicios.addItem(e.getNombre());
        }
    }
    /**
     * @param args the command line arguments
     */
    

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JComboBox<String> Boxejercicios;
    private javax.swing.JTextField Nombre;
    private javax.swing.JMenuItem botonSalir;
    private javax.swing.JButton btResultadoEj;
    private javax.swing.JButton btcancelar;
    private javax.swing.JLabel foto1;
    private javax.swing.JLabel foto2;
    private javax.swing.JButton guardarCambios;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JLabel metaPeso;
    private javax.swing.JLabel metaRepeticiones;
    private javax.swing.JLabel metaTiempo;
    private javax.swing.JTextField peso;
    private javax.swing.JTextField repeticiones;
    private javax.swing.JTextField tiempo;
    // End of variables declaration//GEN-END:variables
    private Rutina nRutina;
    Usuarios nUsuario;
    private static int contador;
    private Ejercicio ejercicio;
    
    Rutina newRutina;

}
