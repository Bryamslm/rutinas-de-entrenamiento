
import java.io.Serializable;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author bryam
 */
public class Ejercicio implements Serializable{
    
    private final String nombre;
    private int repeticiones;
     private int peso;
    private int segundos;
    private String linkFoto1;
    private String linkFoto2;
    
    Ejercicio(String nombre, int repeticiones, int peso, int segundos,  String linkFoto1, String linkFoto2){
        this.nombre=nombre;
        this.repeticiones=repeticiones;
        this.segundos=segundos;
        this.linkFoto1=linkFoto1;
        this.linkFoto2=linkFoto2;
        this.peso=peso;
    }

   
   String getNombre(){
       return nombre;
   }
   int getRepeticiones(){
       return repeticiones;
   }
   int getSegundos(){
       return segundos;
   }
   int getPeso(){
       return peso;
   }
   void setRepeticiones(int cantidad){
       this.repeticiones=cantidad;
   }
   void setPeso(int peso){
       this.peso=peso;
   }
   void setSegundos(int cantidad){
       this.segundos=cantidad;
   }
   String getLinkFoto1(){
       return this.linkFoto1;
   }
   String getLinkFoto2(){
       return this.linkFoto2;
   }
}
