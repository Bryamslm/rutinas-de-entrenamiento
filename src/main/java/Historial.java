
import java.io.Serializable;
import java.util.LinkedList;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author bryam
 */
public class Historial implements Serializable{
    
    private String nombreRutina;
    private LinkedList<Rutina> historial;

    public Historial(String nombre) {
        this.nombreRutina=nombre;
        this.historial=new LinkedList<>();
    }
    
    void setRutina(Rutina r){
        
       historial.add(r);
       
    }
    void setMejorRutina(Rutina r){
       historial.addFirst(r);    
    }
    String getNombre(){
        return this.nombreRutina;
    }
    void setNombre(String nombre){
        this.nombreRutina=nombre;
    }
    
    LinkedList<Rutina> getRutinas(){
        return this.historial;
    }

}
