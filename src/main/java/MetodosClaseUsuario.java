
import java.util.LinkedList;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author bryam
 */
public interface MetodosClaseUsuario {
    public String getUsuario();
    public String getContra();
    public String getNombre();
    public void addRutina(Rutina rutina);
    public void addHistorial(Historial historial);
    public LinkedList<Rutina> getRutinas();
    public LinkedList<Historial> getHistoriales();
    
}
