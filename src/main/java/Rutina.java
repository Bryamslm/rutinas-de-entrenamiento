
import java.io.Serializable;
import java.util.LinkedList;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author bryam
 */
public class Rutina implements Serializable{
    
    private LinkedList<Ejercicio>rutina;
    private String nombre;
    
    Rutina(String nombre){
        rutina=new LinkedList<Ejercicio>();
        this.nombre=nombre;
    }
    public void agregarEjercicio(String nombre, int repeticiones,int peso, int segundos, String linkFoto1, String linkFoto2){
        
        Ejercicio ejercicio= new Ejercicio(nombre, repeticiones, peso, segundos, linkFoto1, linkFoto2);
        rutina.add(ejercicio);
    }
    public void agregarEjercicio(Ejercicio e){
        rutina.add(e);
    }
    
    String getNombreRutina(){
        return this.nombre;
    }
    
    int getCantidadEnLista(){
        return this.rutina.size();
    }
    
    LinkedList<Ejercicio> getEjercicios(){
        return this.rutina;
    }
    void setNombreRutina(String nombre){
        this.nombre=nombre;
    }
    
}
